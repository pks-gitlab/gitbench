#!/usr/bin/env bash

set -xeo pipefail

if test "$#" -ne 4
then
    echo >&2 "USAGE: $0 <repo> <refformat> <existing-refs> <queried-refs>"
    exit 1
fi

repo="$1"
refformat="$2"
existing_refs="$3"
queried_refs="$4"

if test "$queried_refs" -gt "$existing_refs"
then
    echo >&2 "more queried than existing refs"
    exit 1
fi

rm -rf "$repo"
git init "$repo" --ref-format="$refformat" --initial-branch="main"
git -C "$repo" commit --allow-empty --message "root commit"
awk "
    BEGIN {
        print \"start\";
        for (i = 0; i < $existing_refs; i++)
            printf \"create refs/heads/branch-%d HEAD\n\", i;
        print \"commit\";
    }
" | git -C "$repo" update-ref --stdin

git -C "$repo" pack-refs --all

awk "
    BEGIN {
        for (i = 0; i < $queried_refs; i++)
            printf \"refs/heads/branch-%d\n\", $existing_refs / $queried_refs * i;
    }
" >"$repo/input"
